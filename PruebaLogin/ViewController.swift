//
//  ViewController.swift
//  PruebaLogin
//
//  Created by Danni Brito on 14/2/18.
//  Copyright © 2018 Danni Brito. All rights reserved.
//

import UIKit
//import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit

class ViewController: UIViewController {
    @IBOutlet weak var imagenPerfil: UIImageView!
    
    
    
    
    
    
    
    override func viewDidLoad() {
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
//        if let accessToken = AccessToken.current {
//            // User is logged in, use 'accessToken' here.
//        }
//        super.viewDidLoad()
//        let loginButton = LoginButton(readPermissions: [ .publicProfile ])
//        loginButton.center = view.center
//
//        view.addSubview(loginButton)
//        loginButton = LoginButton(readPermissions: [ .publicProfile, .Email, .UserFriends ])
        
        let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"])
        let _ = request?.start(completionHandler: { (connection, result, error) in
            guard let userInfo = result as? [String: Any] else { return } //handle the error
            
            //The url is nested 3 layers deep into the result so it's pretty messy
//            if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
//
//                //downloadImage(url: imageURL)
//                //Download image from imageURL
//            }
        })
        
        if (FBSDKAccessToken.current() != nil) {
                     print("DENTRO")
            //performSegue(withIdentifier: "acceso", sender: self)
            
            
          
            
        }else{
            let loginButton = FBSDKLoginButton()
            let newCenter = CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height - 50)
            loginButton.center = newCenter
            view.addSubview(loginButton)
            performSegue(withIdentifier: "acceso", sender: self)
            
            print("fuera")
            
            //btnIngresar(self)
            
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
        
        
    }
    
    

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("preparando segue")
        
        if segue.identifier == "acceso"{
            print("khe =====>  mostrando")
            
            let destination = segue.destination as? busquedaViewController
            destination?.id="gg"
            
            
        }
    }
    
    
}





